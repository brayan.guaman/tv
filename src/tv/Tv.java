/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tv;

import hardware.Hardware;
import java.util.Scanner;
import software.Software;
//Jarvis

/**
 * 
 * @author Brayan Guaman
 */
public class Tv {
    String tecnologia;
    String color;
    public Hardware hardware;
    public Software software;
    boolean encendido;

    public Tv(){
        this("media","gris");
    }
    public Tv(String tecnologia, String color) {
        //verificar si el contenido enviado es válido
        if(tecnologia.contentEquals("antigua") || tecnologia.contentEquals("media") || tecnologia.contentEquals("moderna")){
            this.tecnologia = tecnologia;
            //color cualquiera
            this.color = color;
            //por defecto apagado
            this.encendido = true;
            
            //creamos hardware
            this.hardware = new Hardware(this.tecnologia);
            
            //creamos software
            this.software = new Software();
            
            
        }
    }
    
    public void encender(){
        encendido = true;
        System.out.println("ENCENDIDO");
    }
    
    public void apagar(){
        encendido = false;
        System.out.println("APAGADO");
    }
    
    public boolean getEncendido(){
    
        return this.encendido;
    }
    
    public void mostrarInformacion(){
        int[] resolucion = this.hardware.pantalla.getResolucion();
        String[] salida =  {
            "Volumen: "+ this.software.auditivo.getVolumen(),
            "Canal: "+ this.software.medio.getNumeroCanal(),
            "Tamaño: " + this.hardware.pantalla.getTamanioPulgadas(),
            "Resolucion: " + resolucion[0]+" x "+resolucion[1],
            "CREDITOS:",
            "Javier Padilla",
            "El Brayan"
        
        };
        
        this.hardware.pantalla.mostrar(salida);
    }
    
    public void mostrarMenu(){
        Scanner ingreso = new Scanner(System.in);
        int opcion;
        
        String[] salida = {
            "Escoja una opción:",
            "1.- Buscar canal",
            "2.- Ingresar Canal",
            "3.- Subir volumen",
            "4.- Bajar Volumen",
            "5.- Salir",
        };
        
        do{
            this.hardware.pantalla.mostrar(salida);
            opcion = ingreso.nextInt();
            
            switch(opcion){
                case 1:
                    this.software.medio.buscar();
                    break;
                case 2:
                    System.out.print("Ingrese el canal: ");
                    int canal = ingreso.nextInt();
                    this.software.medio.setNumeroCanal(canal);
                    break;
                case 3:
                    this.software.auditivo.subirVolumen();
                    break;
                case 4:
                    this.software.auditivo.bajarVolumen();
                    break;
            
            
            }
            
            
        }while(opcion != 5);
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hardware;

/**
 * 
 * @author Brayan Guaman
 */
public class Hardware {
    public Pantalla pantalla;
    public Parlante parlante;

    public Hardware() {
        this("media");
        
    }
//tamaño y resolución establecidos de acuerdo a la tecnología
//Antigua ->
//	-pulgadas: 37
//	-resolucion: 480 x 576
//Media ->
//	-pulgadas: 70
//	-resolucion: 720 x 480
//Moderna->
//	-pulgadas: 86
//	-resolución: 1280 x 720

    public Hardware(String tecnologia){
        
        //de acuerdo a la tecnología creamos el hardware.pantalla
        int[] resolucionAntigua = {480,576};
        int[] resolucionMedia = {720,480};
        int[] resolucionModerna = {1280,720};
        switch(tecnologia){
            case "antigua": 
                this.pantalla = new Pantalla(37 , resolucionAntigua);
                break;
            case "media":
                this.pantalla = new Pantalla(70 , resolucionMedia);
                break;
            case "moderna":
                this.pantalla = new Pantalla(86 , resolucionModerna);
                break;
        }
        
        this.parlante = new Parlante();
        
        
    }
    
    
    
}

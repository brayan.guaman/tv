/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hardware;

import java.util.Arrays;

/**
 * 
 * @author Brayan Guaman
 */
public class Pantalla {
    double tamanioPulgadas;
    int[] resolucion = new int[2];

    public Pantalla(double tamanioPulgadas, int[] resolucion) {
        this.tamanioPulgadas = tamanioPulgadas;
        this.resolucion = resolucion;
    }
    
    //muestra una simulación ASCCI de la salida en pantalla recibiendo un array string
    public void mostrar(String[] datos){
        byte anchoPantalla = 38;
        byte altoPantalla = 17;
        
        String[] salidaPantalla = new String[altoPantalla];
        
        //numero de lineas que sobran
        int lineasRestantesSuperior = (altoPantalla - datos.length)/2;
        //rellenamos el array con cadenas vacias
        Arrays.fill(salidaPantalla,repetirString(" ",anchoPantalla));
        
        
        //verificamos que las lineas no sobrepasen el ancho de pantalla
        for(byte i=0; i< altoPantalla ; i++){
            if(i>=lineasRestantesSuperior && i < (datos.length+lineasRestantesSuperior)){
                //System.out.println(datos[i]);
                int posicionActual = i - lineasRestantesSuperior;
                salidaPantalla[i] = datos[posicionActual].length() < anchoPantalla ? datos[posicionActual] + repetirString(" ", (byte) (anchoPantalla-datos[posicionActual].length())) : 
                        datos[posicionActual].substring(0, anchoPantalla);
            }
            
        }
        
        //dibujamos el tv
        System.out.println(
                "\t ________________________________________\n" +
                "\t|,------------------------------------.  |\\\n"+
                "\t||                                     |=| |");
        for(byte i=0; i< salidaPantalla.length ; i++){
            System.out.println("\t||"+salidaPantalla[i]+"| | |");
        }
        System.out.println(
                "\t|`-------------------------------------' | |\n" + 
                "\t ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ");
        
        
        
    }
    private String repetirString(String cadena, byte repeticiones){
        String cadenaSalida = "";
        for(byte i=1 ; i<repeticiones ; i++){
            cadenaSalida += cadena;
        }
        return cadenaSalida;
    }

    public double getTamanioPulgadas() {
        return tamanioPulgadas;
    }

    public int[] getResolucion() {
        return resolucion;
    }

    

    
    
    
}

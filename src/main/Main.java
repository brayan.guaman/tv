/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package main;

import java.util.Scanner;
import tv.Tv;

/**
 * 
 * @author Brayan Guaman
 */
public class Main {
    
    public static void main(String[] args) {
        Tv televisor = new Tv();
        int opt;
        System.out.println("ENCENDIENDO TV");
        
        
        Scanner ingreso = new Scanner(System.in);
        
        do{
            System.out.println("Para encender/apagar ingrese 1");
            System.out.println("Para mostrar el menú ingrese 2");
            System.out.println("Para mostrar información del tv ingrese 3");
            opt = ingreso.nextInt();
            switch(opt){
                case 1:
                    if (televisor.getEncendido()){ televisor.apagar(); } else { televisor.encender(); }
                    break;
                case 2:
                    if (televisor.getEncendido()){ 
                        televisor.mostrarMenu(); 
                    } else{
                    
                        System.out.println("TV APAGADO");
                    }
                    break;
                case 3:
                    if (televisor.getEncendido()){
                        televisor.mostrarInformacion(); 
                    } else{
                    
                        System.out.println("TV APAGADO");
                    }
                    break;
            
            }
            
            
        
        }while(true);
        
        
        
        
        
        
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package software;

/**
 * 
 * @author Brayan Guaman
 */
public class Auditivo {
    int volumen;
    public Auditivo(){
        this.volumen = 10;
    }
    public boolean subirVolumen(){
        if (volumen <100){
            volumen++;
            return true;
        }
        else{
            return false;
        }
    }
    public boolean bajarVolumen(){
        if (volumen >0){
            volumen--;
            return true;
        }
        else{
            return false;
        }
    }

    public int getVolumen() {
        return volumen;
    }

    
}

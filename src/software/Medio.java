/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package software;

/**
 * 
 * @author Brayan Guaman
 */
public class Medio {
    int numeroCanal;

    public Medio() {
        this.numeroCanal = 1;
    }

    public boolean setNumeroCanal(int numeroCanal) {
        if(numeroCanal > 0 && numeroCanal <=100){
            this.numeroCanal = numeroCanal;
            return true;
        }
        return false;
    }

    public int getNumeroCanal() {
        return numeroCanal;
    }
    
    
    
    public void buscar(){
        this.numeroCanal = (int)(Math.random()*100);
    }
}
